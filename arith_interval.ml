type interval = { inf: int; sup: int}

let make_interval x y =
	if x < y then { inf = x ; sup = y} else { inf= y; sup= x}

let print_interval p =
	Printf.printf "%d %d\n" p.inf p.sup

let operation op a b =
	let res1 = op a.inf b.inf in
	let res2 = op a.inf b.sup in
	let res3 = op a.sup b.inf in
	let res4 = op a.sup b.sup in

	let max4 a b c d =
		let tmp_max1 = max a b in
		let tmp_max2 = max c d in
		max tmp_max1 tmp_max2 in

	let min4 a b c d =
		let tmp_min1 = min a b in
		let tmp_min2 = min c d in
		min tmp_min1 tmp_min2 in

	{
		inf = min4 res1 res2 res3 res4;
		sup = max4 res1 res2 res3 res4
	}


let add a b =
	let add_bis a b =
		a + b in
	operation add_bis a b

let sub a b =
	let sub_bis a b =
		a - b in
	operation sub_bis a b

let mul a b =
	let mul_bis a b =
		a * b in
	operation mul_bis a b


let () =
	print_interval ( add ( make_interval 1 4 ) ( make_interval 5 19) );
	print_interval ( sub ( make_interval 1 4 ) ( make_interval 5 19) );
	print_interval ( mul ( make_interval 1 4 ) ( make_interval 5 19) );