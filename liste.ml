type t = B | N | R 

let rec permute l =
	match l with
	| [] -> []
	| x :: l ->
		( match x with
			| B -> N
			| N -> R
			| R -> B ) :: permute l

let to_string t =
	match t with
	| B -> "B"
	| N -> "N"
	| R -> "R"

let rec print_list l =
	match l with
	| [] -> Printf.printf "%s" ""
	| x :: s -> (
		Printf.printf "%s " (to_string x);
		print_list s;
	)

let rec compteB l =
	match l with
	| [] -> 0
	| x :: s -> ( match x with
			| B -> 1
			| _ -> 0 ) + compteB s

let max_seq l =
	let rec aux mx cp l =
		match l with
		| [] -> max mx cp
		| x :: s -> match x with
				| B -> aux mx (cp+1) s
				| _ -> aux (max mx cp) 0 s 
	in
	aux 0 0 l

(* let rec compteB_court = List.fold_left (fun acc x -> match x with B -> acc + 1 | _ -> acc) *)

let rec findlast l =
		match l with 
		| [] -> B
		| [x] -> x
		| x :: s -> findlast s

let remplace l =
	let rec aux last l =
		match l with
		| [] -> []
		| x :: s -> (if x = B then last else x) :: (aux last s)
	in
	aux (findlast l) l

let () =
	let l = [B;N;B;B;B;B;N;N;R;N] in
	print_list (remplace l)


(*
let replace l =		
	let rec aux l =
		match l with
		| [] -> B, []
		| [x] -> x, [x]
		| x :: l ->
			let y,l = aux l in
			y, (if x = B then y else x) :: l
	in	
	snd (aux l)
	*)