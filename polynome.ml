type monome = {coeff : int; degre : int}

type polynome = monome list

let afficher_m m =
  if m.coeff <> 0 then
    (
      Printf.printf "%d" m.coeff;
      if m.degre <> 0 then
      (
        if m.degre = 1 then
        (
          Printf.printf "X";
        )
        else
        (
          Printf.printf "X^%d" m.degre;
        )
      )
    )

let affiche_p p = 
  let rec aux p =
    match p with
    | [] -> ()
    | x :: s ->
    (
      if x.coeff > 0 then Printf.printf "+" else ();
      afficher_m x;
      aux s;
    ) in

  match p with
    | [] -> ()
    | x :: s ->
    (
      afficher_m x;
      aux s;
    )

let rec derive p =
  match p with 
  | [] -> []
  | x :: s -> ( if x.coeff > 0 then ({coeff=x.coeff * x.degre;degre=x.degre - 1}) else x ) :: (derive s)

(* on suppose que les 2 polynomes sont triés par les degrés dans l'ordre décroissant *)
let rec somme p1 p2 =
  match p1 with
  | [] -> p2
  | x :: s -> 
    (
      match p2 with 
      | [] -> p1
      | y :: t -> if x.degre = y.degre then ( { coeff = x.coeff + y.coeff ; degre = x.degre} :: somme s t )
        else
        (
          if ( x.degre > y.degre ) then ( x :: somme s p2 ) else ( y :: somme p1 t )
        )
    )

let rec evaluer p v =
  let rec pow a n =
    match n with 
    | 0 -> 1
    | _ -> a * pow a (n-1)
  in 

  match p with
  | [] -> 0
  | x :: s -> ((pow v (x.degre)) * x.coeff) + evaluer s v
  
let () = 
  let l1 = [{coeff=3; degre=2};{coeff=4; degre=1};{coeff=5; degre=0}]
  in
  let l2 = [{coeff=4; degre=2}]
  in
  let l = somme l1 l2
  in
  affiche_p l;
  Printf.printf "= %d" (evaluer l 2);